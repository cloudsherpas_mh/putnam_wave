q = load "DS_Hurdle_Summary";
q = filter q by firm_name in {{ selection(cs_step_firm_name) }};
q = filter q by firm_channel in {{ selection(cs_step_channel_name) }};
q = group q by all;
c = filter q by date('week_end_date_Year', 'week_end_date_Month', 'week_end_date_Day') in ["current year".."current day"];
p = filter q by date('py_week_end_date_Year', 'py_week_end_date_Month', 'py_week_end_date_Day') in ["current year".."current day"];
c1 = filter c by hurdle_type == "10K";
c1 = foreach c1 generate sum('hurdle_count') as 'cy_hurdles_10k', sum('hurdle_count') * 0 as 'py_hurdles_10k', sum('hurdle_count') * 0 as 'cy_hurdles_100k', sum('hurdle_count') * 0 as 'py_hurdles_100k';
c2 = filter c by hurdle_type == "100K";
c2 = foreach c2 generate sum('hurdle_count') * 0 as 'cy_hurdles_10k', sum('hurdle_count') * 0 as 'py_hurdles_10k', sum('hurdle_count') as 'cy_hurdles_100k', sum('hurdle_count') * 0 as 'py_hurdles_100k';
p1 = filter p by hurdle_type == "10K";
p1 = foreach p1 generate sum('hurdle_count') * 0 as 'cy_hurdles_10k', sum('hurdle_count') as 'py_hurdles_10k', sum('hurdle_count') * 0 as 'cy_hurdles_100k', sum('hurdle_count') * 0 as 'py_hurdles_100k';
p2 = filter p by hurdle_type == "100K";
p2 = foreach p2 generate sum('hurdle_count') * 0 as 'cy_hurdles_10k', sum('hurdle_count') * 0 as 'py_hurdles_10k', sum('hurdle_count') * 0 as 'cy_hurdles_100k', sum('hurdle_count') as 'py_hurdles_100k';
s = union p1, p2, c1, c2;
s = group s by all;
s = foreach s generate sum('cy_hurdles_10k') / sum('py_hurdles_10k') - 1 as 'avg_10k_yoy_pct',  sum('cy_hurdles_100k') / sum('py_hurdles_100k') - 1 as 'avg_100k_yoy_pct';
